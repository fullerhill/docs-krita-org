# translation of docs_krita_org_reference_manual___main_menu___tools_menu.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___main_menu___tools_menu\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-04-02 12:38+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../reference_manual/main_menu/tools_menu.rst:1
msgid "The tools menu in Krita."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:11
#, fuzzy
#| msgid "Macros"
msgid "Macro"
msgstr "Makrá"

#: ../../reference_manual/main_menu/tools_menu.rst:11
#, fuzzy
#| msgid "Scripting"
msgid "Scripts"
msgstr "Skriptovanie"

#: ../../reference_manual/main_menu/tools_menu.rst:17
msgid "Tools Menu"
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:19
msgid "This contains three things."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:22
msgid "Scripting"
msgstr "Skriptovanie"

#: ../../reference_manual/main_menu/tools_menu.rst:24
msgid ""
"When you have python scripting enabled and have scripts toggled, this is "
"where most scripts are stored by default."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:27
msgid "Recording"
msgstr "Nahrávanie"

#: ../../reference_manual/main_menu/tools_menu.rst:31
msgid "The recording and macro features are unmaintained and buggy."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:33
msgid ""
"Record a macro. You do this by pressing start, drawing something and then "
"pressing stop. This feature can only record brush strokes. The resulting "
"file is stored as a \\*.kritarec file."
msgstr ""

#: ../../reference_manual/main_menu/tools_menu.rst:36
msgid "Macros"
msgstr "Makrá"

#: ../../reference_manual/main_menu/tools_menu.rst:38
msgid ""
"Play back or edit a krita rec file. The edit can only change the brush "
"preset on strokes or add and remove filters."
msgstr ""
