# translation of docs_krita_org_reference_manual___tools___similar_select.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___similar_select\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-03-29 15:00+0100\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 2.0\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<generated>:1
msgid "Fuzziness"
msgstr "Neostrosť"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
#, fuzzy
#| msgid ""
#| ".. image:: images/icons/Krita_mouse_left.png\n"
#| "   :alt: mouseleft"
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:78
msgid ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"
msgstr ""
".. image:: images/icons/similar_select_tool.svg\n"
"   :alt: toolselectsimilar"

#: ../../reference_manual/tools/similar_select.rst:1
msgid "Krita's similar color selection tool reference."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:11
msgid "Selection"
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:11
#, fuzzy
#| msgid "Similar Color Selection Tool"
msgid "Similar Selection"
msgstr "Nástroj výberu podobnej farby"

#: ../../reference_manual/tools/similar_select.rst:16
msgid "Similar Color Selection Tool"
msgstr "Nástroj výberu podobnej farby"

#: ../../reference_manual/tools/similar_select.rst:18
msgid "|toolselectsimilar|"
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:20
msgid ""
"This tool, represented by a dropper over an area with a dashed border, "
"allows you to make :ref:`selections_basics` by selecting a point of color. "
"It will select any areas of a similar color to the one you selected. You can "
"adjust the \"fuzziness\" of the tool in the tool options dock. A lower "
"number will select colors closer to the color that you chose in the first "
"place."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:23
msgid "Hotkeys and Sticky keys"
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:25
msgid ""
":kbd:`R` sets the selection to 'replace' in the tool options, this is the "
"default mode."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:26
msgid ":kbd:`A` sets the selection to 'add' in the tool options."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:27
msgid ":kbd:`S` sets the selection to 'subtract' in the tool options."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:28
msgid ""
":kbd:`Shift +` |mouseleft| sets the subsequent selection to 'add'. You can "
"release the :kbd:`Shift` key while dragging, but it will still be set to "
"'add'. Same for the others."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:29
msgid ":kbd:`Alt +` |mouseleft| sets the subsequent selection to 'subtract'."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:30
msgid ":kbd:`Ctrl +` |mouseleft| sets the subsequent selection to 'replace'."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:31
msgid ""
":kbd:`Shift + Alt +` |mouseleft| sets the subsequent selection to "
"'intersect'."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:35
msgid "Hovering over a selection allows you to move it."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:36
msgid ""
"|mouseright| will open up a selection quick menu with amongst others the "
"ability to edit the selection."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:40
msgid ""
"You can switch the behavior of the :kbd:`Alt` key to use the :kbd:`Ctrl` key "
"instead by toggling the switch in the :ref:`general_settings`."
msgstr ""

#: ../../reference_manual/tools/similar_select.rst:43
msgid "Tool Options"
msgstr "Možnosti nástroja"

#: ../../reference_manual/tools/similar_select.rst:46
msgid ""
"This controls whether or not the contiguous selection sees another color as "
"a border."
msgstr ""
