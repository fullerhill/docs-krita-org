# Translation of docs_krita_org_reference_manual___dockers___animation_docker.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-05 03:40+0200\n"
"PO-Revision-Date: 2019-05-20 16:52+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.1\n"

#: ../../reference_manual/dockers/animation_docker.rst:1
msgid "Overview of the animation docker."
msgstr "Vista general de l'acoblador Animació."

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Animation"
msgstr "Animació"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Animation Playback"
msgstr "Reproduir l'animació"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Play/Pause"
msgstr "Reproduir/Pausa"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Framerate"
msgstr "Velocitat dels fotogrames"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "FPS"
msgstr "FPS"

#: ../../reference_manual/dockers/animation_docker.rst:10
msgid "Speed"
msgstr "Velocitat"

#: ../../reference_manual/dockers/animation_docker.rst:15
msgid "Animation Docker"
msgstr "Acoblador Animació"

#: ../../reference_manual/dockers/animation_docker.rst:18
msgid ".. image:: images/dockers/Animation_docker.png"
msgstr ".. image:: images/dockers/Animation_docker.png"

#: ../../reference_manual/dockers/animation_docker.rst:19
msgid ""
"To have a playback of the animation, you need to use the animation docker."
msgstr ""
"Per a tenir una reproducció de l'animació, haureu d'utilitzar l'acoblador "
"Animació."

#: ../../reference_manual/dockers/animation_docker.rst:21
msgid ""
"The first big box represents the current Frame. The frames are counted with "
"programmer's counting so they start at 0."
msgstr ""
"El primer gran quadre representa el Fotograma actual. Els fotogrames es "
"compten amb el recompte del programador, de manera que es comença per 0."

#: ../../reference_manual/dockers/animation_docker.rst:23
msgid ""
"Then there are two boxes for you to change the playback range here. So, if "
"you want to do a 10 frame animation, set the end to 10, and then Krita will "
"cycle through the frames 0 to 10."
msgstr ""
"Després hi ha dos quadres perquè canvieu aquí l'interval de la reproducció. "
"Després, si voleu fer una animació de 10 fotogrames, establiu el final a 10, "
"i després el Krita farà un cicle a través dels fotogrames del 0 fins al 10."

#: ../../reference_manual/dockers/animation_docker.rst:25
msgid ""
"The bar in the middle is filled with playback options, and each of these can "
"also be hot-keyed. The difference between a keyframe and a normal frame in "
"this case is that a normal frame is empty, while a keyframe is filled."
msgstr ""
"La barra al centre està plena d'opcions per a la reproducció, i cadascuna "
"d'aquestes també pot tenir dreceres. La diferència entre un fotograma clau i "
"un fotograma normal en aquest cas és que un fotograma normal estarà buit, "
"mentre que un fotograma clau estarà ple."

#: ../../reference_manual/dockers/animation_docker.rst:27
msgid ""
"Then, there's buttons for adding, copying and removing frames. More "
"interesting is the next row:"
msgstr ""
"Després, hi ha botons per afegir, copiar i eliminar els fotogrames. Més "
"interessant és la següent fila:"

#: ../../reference_manual/dockers/animation_docker.rst:29
msgid "Onion Skin"
msgstr "Pell de ceba"

#: ../../reference_manual/dockers/animation_docker.rst:30
msgid "Opens the :ref:`onion_skin_docker` if it wasn't open before."
msgstr "Obre l':ref:`onion_skin_docker` si abans no estava oberta."

#: ../../reference_manual/dockers/animation_docker.rst:31
msgid "Auto Frame Mode"
msgstr "Mode automàtic per al fotograma"

#: ../../reference_manual/dockers/animation_docker.rst:32
msgid ""
"Will make a frame out of any empty frame you are working on. Currently "
"automatically copies the previous frame."
msgstr ""
"Crearà un fotograma amb qualsevol fotograma buit en el qual estigueu "
"treballant. Actualment copiarà automàticament el fotograma anterior."

#: ../../reference_manual/dockers/animation_docker.rst:34
msgid "Drop frames"
msgstr "Descarta els fotogrames"

#: ../../reference_manual/dockers/animation_docker.rst:34
msgid ""
"This'll drop frames if your computer isn't fast enough to show all frames at "
"once. This process is automatic, but the icon will become red if it's forced "
"to do this."
msgstr ""
"Això eliminarà fotogrames si el vostre ordinador no és prou ràpid per a "
"mostrar tots els fotogrames alhora. Aquest procés és automàtic, però la "
"icona es tornarà vermella si es força a fer-ho."

#: ../../reference_manual/dockers/animation_docker.rst:36
msgid ""
"You can also set the speedup of the playback, which is different from the "
"framerate."
msgstr ""
"També podreu establir l'acceleració de la reproducció, la qual és diferent "
"de la velocitat dels fotogrames."
