# Translation of docs_krita_org_reference_manual___brushes___brush_engines___dyna_brush_engine.po to Catalan
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Antoni Bella Pérez <antonibella5@yahoo.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: reference_manual\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-03 13:24+0200\n"
"Last-Translator: Antoni Bella Pérez <antonibella5@yahoo.com>\n"
"Language-Team: Catalan <kde-i18n-ca@kde.org>\n"
"Language: ca\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../<generated>:1
msgid "Paint Connection"
msgstr "Pintar la connexió"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:1
msgid "The Dyna Brush Engine manual page."
msgstr "La pàgina del manual per al motor del pinzell dinàmic."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:15
msgid "Dyna Brush Engine"
msgstr "Motor del pinzell dinàmic"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:18
msgid ".. image:: images/icons/dynabrush.svg"
msgstr ".. image:: images/icons/dynabrush.svg"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:19
msgid ""
"Dyna brush uses dynamic setting like mass and drag to draw strokes. The "
"results are fun and random spinning strokes. To experiment more with this "
"brush you can play with values in 'dynamic settings' section of the brush "
"editor under Dyna Brush."
msgstr ""
"El pinzell dinàmic utilitza algun ajustament dinàmic, com la massa i "
"l'arrossegament per a dibuixar els traços. Per a experimentar més amb aquest "
"pinzell, podeu jugar amb els valors a la secció «Ajustaments dinàmics» que "
"hi ha a l'editor de pinzells sota el Pinzell dinàmic."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:23
msgid ""
"This brush engine has been removed in 4.0. This engine mostly had smoothing "
"results that the dyna brush tool has in the toolbox. The stabilizer settings "
"can also give you further smoothing options from the tool options."
msgstr ""
"Aquest motor de pinzell s'ha eliminat en la versió 4.0. Sobretot, tenia uns "
"resultats suavitzats que l'eina del pinzell dinàmic té al quadre d'eines. "
"Els ajustaments de l'estabilitzador també us donaran més opcions per al "
"suavitzat des de l'acoblador Opcions de l'eina."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:26
msgid "Options"
msgstr "Opcions"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:28
msgid ":ref:`option_size_dyna`"
msgstr ":ref:`option_size_dyna`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:29
msgid ":ref:`blending_modes`"
msgstr ":ref:`blending_modes`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:30
msgid ":ref:`option_opacity_n_flow`"
msgstr ":ref:`option_opacity_n_flow`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:31
msgid ":ref:`option_airbrush`"
msgstr ":ref:`option_airbrush`"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:36
msgid "Brush Size (Dyna)"
msgstr "Mida del pinzell (dinàmica)"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:39
msgid "Dynamics Settings"
msgstr "Ajustaments dinàmics"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:41
msgid "Initial Width"
msgstr "Amplada inicial"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:42
msgid "Initial size of the dab."
msgstr "La mida inicial del toc del traç."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:43
msgid "Mass"
msgstr "Massa"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:44
msgid "How much energy there is in the satellite like movement."
msgstr "Quanta energia hi ha al satèl·lit com a moviment."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:45
msgid "Drag"
msgstr "Arrossega"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:46
msgid "How close the dabs follow the position of the brush-cursor."
msgstr ""
"Com de propers estaran els tocs del traç de la posició del cursor del "
"pinzell."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:48
msgid "Width Range"
msgstr "Interval d'amplada"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:48
msgid "How much the dab expands with speed."
msgstr "Quant s'expandirà el toc del traç amb la velocitat."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:51
msgid "Shape"
msgstr "Forma"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:53
msgid "Diameter"
msgstr "Diàmetre"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:54
msgid "Size of the shape."
msgstr "La mida de la forma."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:55
msgid "Angle"
msgstr "Angle"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:56
msgid "Angle of the shape. Requires Fixed Angle active to work."
msgstr ""
"L'angle de la forma. Requereix tenir activada l'opció Angle fix per a que "
"funcioni."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:57
msgid "Circle"
msgstr "Cercle"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:58
msgid "Make a circular dab appear."
msgstr "Fa aparèixer un punter circular per al toc del traç."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:59
msgid "Two"
msgstr "Dos"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:60
msgid "Draws an extra circle between other circles."
msgstr "Dibuixa un cercle addicional entre els altres cercles."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:61
msgid "Line"
msgstr "Línia"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:62
msgid ""
"Connecting lines are drawn next to each other. The number boxes on the right "
"allows you to set the spacing between the lines and how many are drawn."
msgstr ""
"Les línies connectades es dibuixaran una al costat de l'altra. Els quadres "
"numèrics que hi ha a la dreta permeten establir l'espaiat entre les línies i "
"quantes es dibuixaran."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:63
msgid "Polygon"
msgstr "Polígon"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:64
msgid "Draws a black polygon as dab."
msgstr "Dibuixa un polígon negre com a toc del pinzell."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:65
msgid "Wire"
msgstr "Cable"

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:66
msgid "Draws the wireframe of the polygon."
msgstr "Dibuixa un marc de cable del polígon."

#: ../../reference_manual/brushes/brush_engines/dyna_brush_engine.rst:68
msgid "Draws the connection line."
msgstr "Dibuixa la línia de connexió."
