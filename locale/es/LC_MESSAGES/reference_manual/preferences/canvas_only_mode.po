# Spanish translations for docs_krita_org_reference_manual___preferences___canvas_only_mode.po package.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Automatically generated, 2019.
# Eloy Cuadra <ecuadra@eloihr.net>, 2019.
# Sofia Priego <spriego@darksylvania.net>, %Y.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___preferences___canvas_only_mode\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-02-19 19:24+0100\n"
"Last-Translator: Sofia Priego <spriego@darksylvania.net>\n"
"Language-Team: Spanish <kde-l10n-es@kde.org>\n"
"Language: es\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 18.12.2\n"

#: ../../reference_manual/preferences/canvas_only_mode.rst:1
msgid "Canvas only mode settings in Krita."
msgstr "Preferencias del modo de solo lienzo de Krita."

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
#: ../../reference_manual/preferences/canvas_only_mode.rst:16
msgid "Canvas Only Mode"
msgstr "Modo de solo lienzo"

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
msgid "Preferences"
msgstr ""

#: ../../reference_manual/preferences/canvas_only_mode.rst:11
msgid "Settings"
msgstr ""

#: ../../reference_manual/preferences/canvas_only_mode.rst:18
#, fuzzy
#| msgid ""
#| "Canvas Only mode is Krita's version of full screen mode. It is activated "
#| "by hitting TAB on the keyboard. Select which parts of Krita will be "
#| "hidden in canvas-only mode -- The user can set which UI items will be "
#| "hidden in canvas-only mode. Selected items will be hidden."
msgid ""
"Canvas Only mode is Krita's version of full screen mode. It is activated by "
"hitting the :kbd:`Tab` key on the keyboard. Select which parts of Krita will "
"be hidden in canvas-only mode -- The user can set which UI items will be "
"hidden in canvas-only mode. Selected items will be hidden."
msgstr ""
"El modo de solo lienzo es la versión a pantalla completa de Krita. Se activa "
"al pulsar TAB en el teclado. Seleccione las partes de Krita que permanecerán "
"ocultas en el modo de solo lienzo. El usuario puede definir los elementos de "
"la interfaz gráfica que permanecerán ocultos en el modo de solo lienzo. Los "
"elementos seleccionados estarán ocultos."
