# Translation of docs_krita_org_reference_manual___filters___emboss.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___filters___emboss\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-06-11 03:18+0200\n"
"PO-Revision-Date: 2019-06-11 08:50+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.04.0\n"

#: ../../reference_manual/filters/emboss.rst:1
msgid "Overview of the emboss filters."
msgstr "Огляд фільтрів створення рельєфу."

#: ../../reference_manual/filters/emboss.rst:10
#: ../../reference_manual/filters/emboss.rst:15
msgid "Emboss"
msgstr "Барельєф"

#: ../../reference_manual/filters/emboss.rst:10
msgid "Filters"
msgstr "Фільтри"

#: ../../reference_manual/filters/emboss.rst:17
msgid ""
"Filters that are named by the traditional embossing technique. This filter "
"generates highlight and shadows to create an effect which makes the image "
"look like embossed. Emboss filters are usually used in the creation of "
"interesting GUI elements, and mostly used in combination with filter-layers "
"and masks."
msgstr ""
"Фільтри, які названо за традиційною методикою створення рельєфних зображень. "
"Програма створює світлі ділянки і тіні на зображенні для відтворення ефекту, "
"внаслідок якого зображення набуває рельєфного вигляду. Фільтри рельєфу "
"зазвичай використовують для створення цікавих елементів графічного "
"інтерфейсу, здебільшого у поєднанні із шарами фільтрування та масками."

#: ../../reference_manual/filters/emboss.rst:20
msgid "Emboss Horizontal Only"
msgstr "Рельєф тільки по горизонталі"

#: ../../reference_manual/filters/emboss.rst:22
msgid "Only embosses horizontal lines."
msgstr "Виконує створення рельєфу лише для горизонтальних ліній."

#: ../../reference_manual/filters/emboss.rst:25
msgid "Emboss in all Directions"
msgstr "Рельєф у всіх напрямках"

#: ../../reference_manual/filters/emboss.rst:27
msgid "Embosses in all possible directions."
msgstr "Виконує створення рельєфу в усіх напрямках."

#: ../../reference_manual/filters/emboss.rst:30
msgid "Emboss (Laplacian)"
msgstr "Рельєф (Лаплас)"

#: ../../reference_manual/filters/emboss.rst:32
msgid "Uses the laplacian algorithm to perform embossing."
msgstr "Використовує лапласів алгоритм для створення рельєфу."

#: ../../reference_manual/filters/emboss.rst:35
msgid "Emboss Vertical Only"
msgstr "Рельєф тільки по вертикалі"

#: ../../reference_manual/filters/emboss.rst:37
msgid "Only embosses vertical lines."
msgstr "Виконує створення рельєфу лише для вертикальних ліній."

#: ../../reference_manual/filters/emboss.rst:40
msgid "Emboss with Variable depth"
msgstr "Витискання зі змінною глибиною"

#: ../../reference_manual/filters/emboss.rst:42
msgid ""
"Embosses with a depth that can be set through the dialog box shown below."
msgstr ""
"Створити рельєф із глибиною, яку можна встановити за допомогою діалогової "
"панелі, яку показано нижче."

#: ../../reference_manual/filters/emboss.rst:45
msgid ".. image:: images/filters/Emboss-variable-depth.png"
msgstr ".. image:: images/filters/Emboss-variable-depth.png"

#: ../../reference_manual/filters/emboss.rst:47
msgid "Emboss Horizontal and Vertical"
msgstr "Рельєф по горизонталі і вертикалі"

#: ../../reference_manual/filters/emboss.rst:49
msgid "Only embosses horizontal and vertical lines."
msgstr ""
"Виконує створення рельєфу лише для горизонтальних і вертикальних ліній."
