# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-07-19 11:52+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: and Krita pgm ppm pbm PBM PGM\n"

#: ../../<generated>:1
msgid ".ppm"
msgstr ".ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:1
msgid "The PBM, PGM and PPM file formats as exported by Krita."
msgstr "Os formatos de ficheiros PBM, PGM e PPM que são exportados pelo Krita."

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.pbm"
msgstr "*.pbm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.pgm"
msgstr "*.pgm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "*.ppm"
msgstr "*.ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PBM"
msgstr "PBM"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PGM"
msgstr "PGM"

#: ../../general_concepts/file_formats/file_pbgpm.rst:10
msgid "PPM"
msgstr "PPM"

#: ../../general_concepts/file_formats/file_pbgpm.rst:17
msgid "\\*.pbm, \\*.pgm and \\*.ppm"
msgstr "\\*.pbm, \\*.pgm e \\*.ppm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:18
msgid ""
"``.pbm``, ``.pgm`` and ``.ppm`` are a series of file-formats with a similar "
"logic to them. They are designed to save images in a way that the result can "
"be read as an ASCII file, from back when email clients couldn't read images "
"reliably."
msgstr ""
"O ``.pbm``, o ``.pgm`` e o ``.ppm`` são uma série de formatos de ficheiros "
"com uma lógica semelhante entre si. Estão desenhados para gravar imagens "
"numa forma em que o resultado possa ser lido como um ficheiro ASCII, que vem "
"do tempo em que os clientes de e-mail não conseguiam ler as imagens de forma "
"fidedigna."

#: ../../general_concepts/file_formats/file_pbgpm.rst:20
msgid ""
"They are very old file formats, and not used outside of very specialized "
"usecases, such as embedding images inside code."
msgstr ""
"Estes são formatos muito antigos e não são usados fora de casos de uso muito "
"específicos, como a incorporação de imagens dentro de código."

#: ../../general_concepts/file_formats/file_pbgpm.rst:22
msgid ".pbm"
msgstr ".pbm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:23
msgid "One-bit and can only show strict black and white."
msgstr "Um bit e só consegue mostrar preto e branco."

#: ../../general_concepts/file_formats/file_pbgpm.rst:24
msgid ".pgm"
msgstr ".pgm"

#: ../../general_concepts/file_formats/file_pbgpm.rst:25
msgid "Can show 255 values of gray (8bit)."
msgstr "Consegue mostrar 255 valores de cinzento (8 bits)."

#: ../../general_concepts/file_formats/file_pbgpm.rst:27
msgid "Can show 8bit rgb values."
msgstr "Consegue mostrar valores RGB de 8 bits."
