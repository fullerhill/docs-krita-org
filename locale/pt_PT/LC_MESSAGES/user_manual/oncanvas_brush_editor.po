# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:11+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: HUD Oncanvasbrusheditor en Krita image kbd images\n"
"X-POFile-SpellExtra: guilabel\n"

#: ../../user_manual/oncanvas_brush_editor.rst:1
msgid "Using the oncanvas brush editor in Krita."
msgstr "Uso do editor de pincéis na área de desenho do Krita."

#: ../../user_manual/oncanvas_brush_editor.rst:11
msgid "Brush Settings"
msgstr "Configuração do Pincel"

#: ../../user_manual/oncanvas_brush_editor.rst:11
msgid "Pop-up Palette"
msgstr "Paleta"

#: ../../user_manual/oncanvas_brush_editor.rst:16
msgid "On-Canvas Brush Editor"
msgstr "Editor de Pincéis na Área de Desenho"

#: ../../user_manual/oncanvas_brush_editor.rst:19
msgid ""
"Krita's brush editor is, as you may know, on the :kbd:`F5` key. However, "
"sometimes you just want to modify a single parameter quickly. Perhaps even "
"in canvas-only mode. The on canvas brush editor or brush HUD allows you to "
"do this. It's accessible from the pop-up palette, by ticking the lower-right "
"arrow button."
msgstr ""
"O editor de pincéis do Krita é, como pode saber, activado com o :kbd:`F5`. "
"Contudo, em alguns casos poderá querer modificar apenas um único parâmetro "
"rapidamente - talvez até no modo apenas com a área de desenho. O editor de "
"pincéis na área de desenho ou HUD de pincéis permite-lhe fazê-lo. Está "
"acessível a partir da área da paleta, assinalando para tal o botão da seta "
"inferior direita."

#: ../../user_manual/oncanvas_brush_editor.rst:26
msgid ".. image:: images/On_canvas_brush_editor.png"
msgstr ".. image:: images/On_canvas_brush_editor.png"

#: ../../user_manual/oncanvas_brush_editor.rst:27
msgid ""
"You can change the amount of visible settings and their order by clicking "
"the settings icon next to the brush name."
msgstr ""
"Poderá alterar a quantidade de definições visíveis e a sua ordem se carregar "
"no ícone de configurações a seguir ao nome do pincel."

#: ../../user_manual/oncanvas_brush_editor.rst:31
msgid ".. image:: images/On_canvas_brush_editor_2.png"
msgstr ".. image:: images/On_canvas_brush_editor_2.png"

#: ../../user_manual/oncanvas_brush_editor.rst:32
msgid ""
"On the left are all unused settings, on the right are all used settings. You "
"use the :guilabel:`>` and :guilabel:`<` buttons to move a setting between "
"the two columns. The :guilabel:`Up` and :guilabel:`Down` buttons allow you "
"to adjust the order of the used settings, for when you think flow is more "
"important than size."
msgstr ""
"À esquerda encontram-se todas as definições não usadas; por outro lado, do "
"lado direito, estão todas as usadas. Poderá usar os botões das setas :"
"guilabel:`>` e :guilabel:`<` para mover uma dada definição entre as duas "
"colunas. Os botões :guilabel:`Subir` e :guilabel:`Descer` também lhe "
"permitem ajustar a ordem das definições usadas, para o caso em que pense que "
"o fluxo é mais importante que o tamanho."

#: ../../user_manual/oncanvas_brush_editor.rst:38
msgid ".. image:: images/On_canvas_brush_editor_3.png"
msgstr ".. image:: images/On_canvas_brush_editor_3.png"

#: ../../user_manual/oncanvas_brush_editor.rst:39
msgid ""
"These set-ups are PER brush engine, so different brush engines can have "
"different configurations."
msgstr ""
"Estas configurações são do motor de pincéis PER; como tal, diferentes "
"motores de pincéis poderão ter diferentes configurações."
