# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-03 14:07+0100\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en guilabel image SeparateImage menuselection kbd\n"
"X-POFile-SpellExtra: images vs Inch DPI Dots canvas PPI XXX program\n"
"X-POFile-SpellExtra: blendingmodes Infinite ref ResizeCanvas\n"
"X-POFile-SpellExtra: documentinformationscreen posters Paint Krita JPG\n"
"X-POFile-SpellExtra: croptool ScaleImagetoNewSize kra Tool\n"

#: ../../user_manual/working_with_images.rst:None
msgid ".. image:: images/Infinite-canvas.png"
msgstr ".. image:: images/Infinite-canvas.png"

#: ../../user_manual/working_with_images.rst:1
msgid "Detailed steps on how images work in Krita"
msgstr "Passos detalhados sobre como funcionam as imagens no Krita"

#: ../../user_manual/working_with_images.rst:12
#: ../../user_manual/working_with_images.rst:58
msgid "Metadata"
msgstr "Meta-dados"

#: ../../user_manual/working_with_images.rst:12
msgid "Image"
msgstr "Imagem"

#: ../../user_manual/working_with_images.rst:12
msgid "Document"
msgstr "Documento"

#: ../../user_manual/working_with_images.rst:12
msgid "Raster"
msgstr "Rasterização"

#: ../../user_manual/working_with_images.rst:12
msgid "Vector"
msgstr "Vector"

#: ../../user_manual/working_with_images.rst:17
msgid "Working with Images"
msgstr "Lidar com Imagens"

#: ../../user_manual/working_with_images.rst:19
msgid ""
"Computers work with files and as a painting program, Krita works with images "
"as the type of file it creates and manipulates."
msgstr ""
"Os computadores lidam com ficheiros e, como um programa de pintura, o Krita "
"funciona com imagens como o tipo de ficheiro que cria e manipula."

#: ../../user_manual/working_with_images.rst:23
msgid "What do Images Contain?"
msgstr "O que Contêm as Imagens?"

#: ../../user_manual/working_with_images.rst:25
msgid ""
"If you have a text document, it of course contains letters, strung in the "
"right order, so the computer loads them as coherent sentences."
msgstr ""
"Sr tiver um documento de texto, ele irá conter letras dispostas pela ordem "
"correcta, pelo que o computador carrega-as como frases coerentes."

#: ../../user_manual/working_with_images.rst:29
msgid "Raster Data"
msgstr "Dados Rasterizados"

#: ../../user_manual/working_with_images.rst:31
msgid ""
"This is the main data on the paint layers you make. So these are the strokes "
"with the paint brush and look pixely up close. A multi-layer file will "
"contain several of such layers, that get overlaid on top of each other so "
"make the final image."
msgstr ""
"Estes são os dados principais das camadas de pintura que criar. Como tal, "
"estes são os traços com o pincel de pintura e parecendo realmente próximos. "
"Um ficheiro multi-camadas irá conter diversas camadas como esta, as quais "
"vão sendo sobrepostos para compor a imagem final."

#: ../../user_manual/working_with_images.rst:36
msgid "A single layer file will usually only contain raster data."
msgstr ""
"Um ficheiro com uma única camada só irá conter normalmente dados "
"rasterizados."

#: ../../user_manual/working_with_images.rst:39
msgid "Vector Data"
msgstr "Dados Vectoriais"

#: ../../user_manual/working_with_images.rst:41
msgid ""
"These are mathematical operations that tell the computer to draw pixels on a "
"spot. This makes them much more scalable, because you just tell the "
"operation to make the coordinates 4 times bigger to scale it up. Due to this "
"vector data is much more editable, lighter, but at the same time it's also "
"much more CPU intensive."
msgstr ""
"Estas são operações matemáticas que dizem ao computador para desenhar pixels "
"num dado ponto. Isto torna-as muito mais escaláveis, porque simplesmente diz "
"à operação para tornar as coordenadas 4 vezes maiores para ampliar o "
"resultado. Devido a isto, os dados vectoriais são muito mais editáveis, "
"leves, mas também são ao mesmo tempo mais intensivos a nível de CPU."

#: ../../user_manual/working_with_images.rst:48
msgid "Operation Data"
msgstr "Dados das Operações"

#: ../../user_manual/working_with_images.rst:50
msgid ""
"Stuff like the filter layers, that tells Krita to change the colors of a "
"layer, but also transparency masks, group layer and transformation masks are "
"saved to multi-layer files. Being able to load these depend on the software "
"that initially made the file. So Krita can load and save groups, "
"transparency masks and layer effects from PSD, but not load or save "
"transform masks."
msgstr ""
"As coisas como as camadas de filtragem, que indicam ao Krita como mudar as "
"cores de uma camada, mas também as máscaras de transparência, as camadas de "
"grupos e as máscaras de transformação são gravadas em ficheiros multi-"
"camadas. Ser capaz de carregar estas dependem da aplicação que criou "
"inicialmente o ficheiro. Por isso, o Krita consegue carregar e gravar os "
"grupos, as máscaras de transparência e os efeitos de camadas a partir do "
"PSD, mas não carregar ou gravar as máscaras de transformação."

#: ../../user_manual/working_with_images.rst:60
msgid ""
"Metadata is information like the creation date, author, description and also "
"information like DPI."
msgstr ""
"Os meta-dados são dados como a data de criação, o autor, a descrição e mais "
"algumas informações, como o número de pontos por polegada (PPP)."

#: ../../user_manual/working_with_images.rst:64
msgid "Image size"
msgstr "Tamanho da imagem"

#: ../../user_manual/working_with_images.rst:66
msgid ""
"The image size is the dimension and resolution of the canvas. Image size has "
"direct effect file size of the Krita document. The more pixels that need to "
"be remembered and the higher the bit depth of the color, the heavier the "
"resulting file will be."
msgstr ""
"O tamanho da imagem são as dimensões e a resolução da área de desenho. O "
"tamanho da imagem tem um efeito directo sobre o tamanho do ficheiro do "
"documento do Krita. Quanto mais pixels precisarem de ser recordados e maior "
"a profundidade da cor em bits, maior será o ficheiro resultante."

#: ../../user_manual/working_with_images.rst:72
msgid "DPI/PPI"
msgstr "DPI/PPI"

#: ../../user_manual/working_with_images.rst:74
msgid ""
"**DPI** stands for *Dots per Inch*, **PPI** stands for *Pixels per Inch*. In "
"printing industry, suppose if your printer prints at 300 **DPI**. It means "
"it is actually putting 300 dots of colors in an area equal to an Inch. This "
"means the number of pixels your artwork has in a relative area of an inch."
msgstr ""
"**DPI** significa *Dots per Inch* (Pontos por Polegada), **PPI** significa "
"*Pixels per Inch* (Pixels por Polegada). Na indústria da impressão, suponha "
"que a sua impressora imprime a 300 **DPI**. Isso significa que irá colocar "
"de facto 300 pontos de cores numa área equivalente a uma polegada. Isto "
"significa que o número de pixels que a sua obra tem ocupa uma área relativa "
"de uma polegada."

#: ../../user_manual/working_with_images.rst:80
msgid ""
"**DPI** is the concern of the printer, and artists while creating artwork "
"should keep **PPI** in mind. According to the **PPI** you have set, the "
"printers can decide how large your image should be on a piece of paper."
msgstr ""
"Os **DPI** dizem respeito à impressora e os artistas, quando criam as suas "
"obras, deverão ter os **PPI** em mente. De acordo com os **PPI** que tiver "
"definido, as impressoras poderão decidir quão grande ficará a sua imagem num "
"pedaço de papel."

#: ../../user_manual/working_with_images.rst:85
msgid "Some standards:"
msgstr "Algumas normas:"

#: ../../user_manual/working_with_images.rst:88
msgid ""
"This is the default PPI of monitors as assumed by all programs. It is not "
"fully correct, as most monitors these days have 125 PPI or even 300 PPI for "
"the retina devices. None the less, when making an image for computer "
"consumption, this is the default."
msgstr ""
"Este é o valor de PPI (pixels por polegada) dos monitores que é assumido por "
"todos os programas. Não está completamente correcto, dado que a maioria dos "
"monitores dos dias de hoje têm 125 PPI ou mesmo 300 PPI para os dispositivos "
"Retina. Apesar disso, ao criar uma imagem para ser consumida pelo "
"computador, este é o valor por omissão."

#: ../../user_manual/working_with_images.rst:90
msgid "72 PPI"
msgstr "72 PPI"

#: ../../user_manual/working_with_images.rst:92
msgid "120 PPI"
msgstr "120 PPI"

#: ../../user_manual/working_with_images.rst:93
msgid "This is often used as a standard for low-quality posters."
msgstr ""
"Isto é normalmente usado como norma para os posters de baixa qualidade."

#: ../../user_manual/working_with_images.rst:94
msgid "300 PPI"
msgstr "300 PPI"

#: ../../user_manual/working_with_images.rst:95
msgid "This is the minimum you should use for quality prints."
msgstr "Isto é o mínimo que deverá usar para as impressões com qualidade."

#: ../../user_manual/working_with_images.rst:97
msgid "600 PPI"
msgstr "600 PPI"

#: ../../user_manual/working_with_images.rst:97
msgid "The quality used for line art for comics."
msgstr "A qualidade usada no desenho das linhas em bandas desenhadas."

#: ../../user_manual/working_with_images.rst:100
msgid "Color depth"
msgstr "Profundidade de cor"

#: ../../user_manual/working_with_images.rst:102
msgid ""
"We went over color depth in the :ref:`Color Management page "
"<general_concept_color>`. What you need to understand is that Krita has "
"image color spaces, and layer color spaces, the latter which can save memory "
"if used right. For example, having a line art layer in grayscale can half "
"the memory costs."
msgstr ""
"Já mencionámos a profundidade de cor na :ref:`página de Gestão das Cores "
"<general_concept_color>`. O que precisa de compreender é que o Krita tem "
"espaços de cores das imagens e das camadas, sendo que o último pode poupar "
"memória se for bem usado. Por exemplo, ter uma camada de desenho de linhas "
"em tons de cinzento poderá reduzir a metade os custos de memória."

#: ../../user_manual/working_with_images.rst:108
msgid "Image color space vs layer color space vs conversion."
msgstr "Espaço de cores da imagem vs espaço de cores da camada vs conversão."

#: ../../user_manual/working_with_images.rst:110
msgid ""
"Because there's a difference between image color space and layer color "
"space, you can change only the image color space in :menuselection:`Image --"
"> Properties` which will leave the layers alone. But if you want to change "
"the color space of the file including all the layers you can do it by going "
"to :menuselection:`Image --> Convert Image Color Space` this will convert "
"all the layers color space as well."
msgstr ""
"Dado que existe uma diferença entre o espaço de cores da imagem e o espaço "
"de cores das camadas, poderá alterar apenas o espaço de cores da imagem em :"
"menuselection:`Imagem --> Propriedades`, o qual irá deixar as camadas em "
"paz. Mas se quiser alterar o espaço de cores do ficheiro, incluindo todas as "
"camadas, podê-lo-á fazer se for à opção :menuselection:`Imagem --> Converter "
"o Espaço de Cores da Imagem`, o que irá converter também o espaço de cores "
"de todas as camadas."

#: ../../user_manual/working_with_images.rst:116
msgid "Author and Description"
msgstr "Autor e Descrição"

#: ../../user_manual/working_with_images.rst:119
msgid ".. image:: images/document_information_screen.png"
msgstr ".. image:: images/document_information_screen.png"

#: ../../user_manual/working_with_images.rst:120
msgid ""
"Krita will automatically save who created the image into your image's "
"metadata. Along with the other data such as time and date of creation and "
"modification, Krita also shows editing time of a document in the document "
"information dialog, useful for professional illustrators, speed-painters to "
"keep track of the time they worked on artwork for billing purposes. It "
"detects when you haven’t performed actions for a while, and has a precision "
"of ±60 seconds. You can empty it in the document info dialog and of course "
"by unzipping you .kra file and editing the metadata there."
msgstr ""
"O Krita irá gravar automaticamente quem criou a imagem nos meta-dados da sua "
"imagem. Em conjunto com os outros dados, como a data e hora da criação e "
"modificação, o Krita também apresenta a hora de edição de um documento na "
"janela de informação do documento, o que pode ser útil para os ilustradores "
"profissionais ou pintores eficientes para manter um registo do tempo que "
"gastaram a fazer uma obra, para fins de facturação. Ele detecta quando não "
"efectuou nenhuma acção durante algum tempo, e tem uma precisão de ±60 "
"segundos. Poderá limpar este valor na janela de informação do documento e, "
"como é óbvio, ao descomprimir o seu ficheiro .kra e editando os meta-dados "
"dentro dele."

#: ../../user_manual/working_with_images.rst:130
msgid ""
"These things can be edited in :menuselection:`File --> Document "
"Information`, and for the author's information :menuselection:`Settings --> "
"Configure Krita --> Author Information`. Profiles can be switched under :"
"menuselection:`Settings --> Active Author Profile`."
msgstr ""
"Estas coisas podem ser editadas em :menuselection:`Ficheiro --> Informação "
"do Documento` e, para os dados do autor, em :menuselection:`Configuração --> "
"Configurar o Krita --> Informação do Autor`. Poderá mudar de perfis em :"
"menuselection:`Configuração --> Perfil do Autor Activo`."

#: ../../user_manual/working_with_images.rst:133
msgid "Setting the canvas background color"
msgstr "Configurar a cor de fundo da área de desenho"

#: ../../user_manual/working_with_images.rst:135
msgid ""
"You can set the canvas background color via :menuselection:`Image --> Image "
"Background Color and Transparency`. This allows you to turn the background "
"color non-transparent and to change the color. This is also useful for "
"certain file formats which force a background color instead of transparency. "
"PNG and JPG export use this color as the default color to fill in "
"transparency if you do not want to export transparency."
msgstr ""
"Poderá configurar a cor de fundo da área de desenho com a opção :"
"menuselection:`Imagem --> Cor de Fundo e Transparência da Imagem`. Isto "
"permite-lhe tornar a cor de fundo não transparente e mudar a usa cor. Isto "
"também é útil para certos formatos de ficheiros que obrigam a usar uma cor "
"de fundo em vez de transparência. A exportação para PNG e JPG usam esta cor "
"como predefinida para preencher a transparência, caso não queira exportar a "
"transparência."

#: ../../user_manual/working_with_images.rst:142
msgid ""
"If you come in from a program like :program:`Paint Tool Sai`, then using "
"this option, or using :guilabel:`Set Canvas Background Color` in the new "
"file options, will allow you to work in a slightly more comfortable "
"environment, where transparency isn't depicted with checkered boxes."
msgstr ""
"Se vier de um programa como o :program:`Paint Tool Sai`, então ao usar esta "
"opção ou ao usar a opção :guilabel:`Configurar a Cor de Fundo da Área de "
"Desenho` nas novas opções do ficheiro, poderá trabalhar num ambiente "
"ligeiramente mais confortável, onde a transparência não está representada "
"por padrões em xadrez."

#: ../../user_manual/working_with_images.rst:148
msgid "Basic transforms"
msgstr "Transformações básicas"

#: ../../user_manual/working_with_images.rst:150
msgid "There are some basic transforms available in the image menu."
msgstr "Existem algumas transformações básicas disponíveis no menu da imagem."

#: ../../user_manual/working_with_images.rst:152
msgid "Shear Image"
msgstr "Inclinar Imagem"

#: ../../user_manual/working_with_images.rst:153
msgid "This will allow you to skew the whole image and its layers."
msgstr "Isto permitir-lhe-á inclinar a imagem toda, bem como as suas camadas."

#: ../../user_manual/working_with_images.rst:154
msgid "Rotate"
msgstr "Rodar"

#: ../../user_manual/working_with_images.rst:155
msgid "This will allow you to rotate the image and all its layers quickly."
msgstr "Isto permite-lhe rodar rapidamente a imagem e todas as suas camadas."

#: ../../user_manual/working_with_images.rst:157
msgid "Mirror Horizontal/Vertical"
msgstr "Espelho Horizontal/Vertical"

#: ../../user_manual/working_with_images.rst:157
msgid "This will allow you to mirror the whole image with all its layers."
msgstr ""
"Isto permite-lhe criar um espelho de toda a imagem e todas as suas camadas."

#: ../../user_manual/working_with_images.rst:159
msgid "But there are more options than that..."
msgstr "Mas existem mais opções para além destas..."

#: ../../user_manual/working_with_images.rst:162
msgid "Cropping and resizing the canvas"
msgstr "Recortar e dimensionar a área de desenho"

#: ../../user_manual/working_with_images.rst:164
msgid ""
"You can crop and image with the :ref:`crop_tool`, to cut away extra space "
"and improve the composition."
msgstr ""
"Poderá recortar uma imagem com a :ref:`crop_tool`, para cortar o espaço "
"extra e melhorar a composição."

#: ../../user_manual/working_with_images.rst:168
msgid "Trimming"
msgstr "Recorte"

#: ../../user_manual/working_with_images.rst:170
msgid ""
"Using :menuselection:`Image --> Trim to Layer`, Krita resizes the image to "
"the dimensions of the layer selected. Useful for when you paste a too large "
"image into the layer and want to resize the canvas to the extent of this "
"layer."
msgstr ""
"Se usar a opção :menuselection:`Imagem --> Recortar à Camada`, o Krita "
"dimensiona a imagem para as dimensões da camada seleccionada. Isto é útil "
"quando colar uma imagem demasiado grande para uma camada e quiser "
"dimensionar a área de desenho para a extensão desta camada."

#: ../../user_manual/working_with_images.rst:174
msgid ""
":menuselection:`Image --> Trim to Selection` is a faster cousin to the crop "
"tool. This helps us to resize the canvas to the dimension of any active "
"selection. This is especially useful with right clicking the layer on the "
"layer stack and choosing :guilabel:`Select Opaque`. :menuselection:`Image --"
"> Trim to Selection` will then crop the canvas to the selection bounding box."
msgstr ""
"A :menuselection:`Imagem --> Recortar à Selecção` é uma versão mais rápida "
"da ferramenta de recorte. Esta ajuda-nos a dimensionar a área de desenho à "
"dimensão de qualquer selecção activa. Isto é especialmente útil se carregar "
"com o botão direito sobre a camada na pilha de camadas e escolher :guilabel:"
"`Selecção Opaca`. A opção :menuselection:`Imagem --> Recortar à Selecção` "
"irá então recortar a área de desenho à área envolvente da selecção."

#: ../../user_manual/working_with_images.rst:180
msgid ""
":menuselection:`Image --> Trim to Image Size` is actually for layers, and "
"will trim all layers to the size of the image, making your files lighter by "
"getting rid of invisible data."
msgstr ""
"A :menuselection:`Imagem --> Recortar ao Tamanho da Imagem` é de facto para "
"camadas, recortando todas as camadas ao tamanho da imagem, tornando os seus "
"ficheiros mais leves ao eliminar os dados invisíveis."

#: ../../user_manual/working_with_images.rst:185
msgid "Resizing the canvas"
msgstr "Dimensionar a área de desenho"

#: ../../user_manual/working_with_images.rst:187
msgid ""
"You can also resize the canvas via :menuselection:`Image --> Resize Canvas` "
"(or the :kbd:`Ctrl + Alt + C` shortcut). The dialog box is shown below."
msgstr ""
"Poderá também dimensionar a área de desenho com a opção :menuselection:"
"`Imagem --> Dimensionar a Área de Desenho` (ou :kbd:`Ctrl + Alt + C`). A "
"janela aparece abaixo."

#: ../../user_manual/working_with_images.rst:191
msgid ".. image:: images/Resize_Canvas.png"
msgstr ".. image:: images/Resize_Canvas.png"

#: ../../user_manual/working_with_images.rst:192
msgid ""
"In this, constraint proportions will make sure the height and width stay in "
"proportion to each other as you change them. Offset indicates where the new "
"canvas space is added around the current image. You basically decide where "
"the current image goes (if you press the left-button, it'll go to the center "
"left, and the new canvas space will be added to the right of the image)."
msgstr ""
"Neste caso, as proporções restritas irão garantir que a altura e a largura "
"mantêm-se proporcionais entre si, à medida que as vai alterando. O "
"deslocamento indica onde será adicionado o novo espaço da área de desenho em "
"torno da imagem actual. Basicamente poderá decidir onde vai ficar a imagem "
"actual (se carregar com o botão esquerdo, irá para o centro à esquerda, e o "
"novo espaço da área de desenho será adicionado à direita da imagem)."

#: ../../user_manual/working_with_images.rst:199
msgid ""
"Another way to resize the canvas according to the need while drawing is when "
"you scroll away from the end of the canvas, you can see an arrow appear. "
"Clicking this will extend the canvas in that direction. You can see the "
"arrow marked in red in the example below:"
msgstr ""
"Outra forma de dimensionar a área de desenho enquanto desenha, de acordo com "
"as necessidades, é quando se desloca para lá do fim da área de desenho, onde "
"poderá ver uma seta a aparecer. Se carregar nesta, irá alargar a área de "
"desenho nessa direcção. Poderá ver a seta marcada a vermelho no exemplo "
"abaixo:"

#: ../../user_manual/working_with_images.rst:209
msgid "Resizing the image"
msgstr "Dimensionar a imagem"

#: ../../user_manual/working_with_images.rst:211
msgid ""
":guilabel:`Scale Image to New Size` allows you to resize the whole image. "
"Also, importantly, this is where you can change the resolution or *upres* "
"your image. So for instance, if you were initially working at 72 PPI to "
"block in large shapes and colors, images, etc... And now you want to really "
"get in and do some detail work at 300 or 400 PPI this is where you would "
"make the change."
msgstr ""
"A opção :guilabel:`Escalar a Imagem para um Novo Tamanho` permite-lhe "
"dimensionar toda a imagem. Também, o que é importante, é aqui que poderá "
"alterar a resolução ou *aumentar a resolução* da sua imagem. Por isso, se "
"por exemplo estivesse a trabalhar inicialmente a 72 PPI para imprimir "
"grandes formas e cores, imagens, etc... E se agora realmente precisar de "
"fazer algum trabalho de detalhe a 300 ou 400 PPI, é aqui que iria fazer a "
"alteração."

#: ../../user_manual/working_with_images.rst:218
msgid ""
"Like all other dialogs where a chain link appears, when the chain is linked "
"the aspect ratio is maintained. To disconnect the chain, just click on the "
"links and the two halves will separate."
msgstr ""
"Como todas as outras janelas onde aparece um elo de corrente, quando esta "
"corrente está ligada, as proporções de tamanho são mantidas. Para desligar a "
"corrente, basta carregar na mesma e as duas metades separar-se-ão."

#: ../../user_manual/working_with_images.rst:224
msgid ".. image:: images/Scale_Image_to_New_Size.png"
msgstr ".. image:: images/Scale_Image_to_New_Size.png"

#: ../../user_manual/working_with_images.rst:226
msgid "Separating Images"
msgstr "Separar as Imagens"

#: ../../user_manual/working_with_images.rst:229
msgid ".. image:: images/Separate_Image.png"
msgstr ".. image:: images/Separate_Image.png"

#: ../../user_manual/working_with_images.rst:230
msgid ""
"This powerful image manipulation feature lets you separate an image into its "
"different components or channels."
msgstr ""
"Esta funcionalidade poderosa de manipulação da imagem permite-lhe separar "
"uma imagem nas suas diferentes componentes ou canais."

#: ../../user_manual/working_with_images.rst:233
msgid ""
"This is useful for people working in print, or people manipulating game "
"textures. There's no combine functionality, but what you can do, if using "
"colored output, is to set two of the channels to the addition :ref:"
"`blending_modes`."
msgstr ""
"Isto é útil para as pessoas que trabalham em impressões ou que manipulam "
"texturas de jogos. Não existe nenhuma funcionalidade de combinação, mas o "
"que poderá fazer, caso esteja a usar resultados a cores é atribuir dois dos "
"canais aos :ref:`blending_modes` de adição."

#: ../../user_manual/working_with_images.rst:238
msgid ""
"For grayscale images in the RGB space, you can use the copy red, copy green "
"and copy blue blending modes, with using the red one for the red channel "
"image, etc."
msgstr ""
"Para as imagens em tons de cinzento no espaço RGB, poderá usar os modos de "
"mistura para copiar o vermelho, o verde ou o azul."

#: ../../user_manual/working_with_images.rst:243
msgid "Saving, Exporting and Opening Files"
msgstr "Gravar, Exportar e Abrir os Ficheiros"

#: ../../user_manual/working_with_images.rst:245
msgid ""
"When Krita creates or opens a file, it has a copy of the file in memory, "
"that it edits. This is part of the way how computers work: They make a copy "
"of their file in the RAM. Thus, when saving, Krita takes its copy and copies "
"it over the existing file. There's a couple of tricks you can do with saving."
msgstr ""
"Quando o Krita cria ou abre um ficheiro, ele fica com uma cópia do mesmo em "
"memória para o poder editar. Isto é parte da forma como funcionam os "
"computadores: Eles criam uma cópia do seu ficheiro na RAM. Depois, ao "
"gravar, o Krita pega na sua cópia e envia-a de volta para o ficheiro "
"existente. Existe um conjunto de truques que poderá usar na gravação."

#: ../../user_manual/working_with_images.rst:252
msgid ""
"Krita saves the current image in its memory to a defined place on the hard-"
"drive. If the image hadn't been saved before, Krita will ask you where to "
"save it."
msgstr ""
"O Krita grava a imagem na sua memória para um local bem definido no seu "
"disco rígido. Se a imagem não tiver sido gravada antes, o Krita perguntar-"
"lhe-á onde deseja gravá-la."

#: ../../user_manual/working_with_images.rst:253
msgid "Save"
msgstr "Gravar"

#: ../../user_manual/working_with_images.rst:256
msgid "Save As"
msgstr "Gravar Como"

#: ../../user_manual/working_with_images.rst:256
msgid ""
"Make a copy of your current file by saving it with a different name. Krita "
"will switch to the newly made file as its active document."
msgstr ""
"Cria uma cópia do seu ficheiro actual, gravando-a com um nome diferente. O "
"Krita irá mudar para o novo ficheiro criado, usando-o como o seu documento "
"activo."

#: ../../user_manual/working_with_images.rst:258
msgid "Open"
msgstr "Abrir"

#: ../../user_manual/working_with_images.rst:259
msgid "Open a saved file. Fairly straightforward."
msgstr "Abre um ficheiro gravado. Tão simples como isto."

#: ../../user_manual/working_with_images.rst:261
msgid ""
"Save a file to a new location without actively opening it. Useful for when "
"you are working on a layered file, but only need to save a flattened version "
"of it to a certain location."
msgstr ""
"Grava um ficheiro num novo local sem ter de o abrir de facto. Isto é útil "
"quando estiver a trabalhar num ficheiro em camadas, mas só precisar de "
"gravar uma versão plana do mesmo num dado local."

#: ../../user_manual/working_with_images.rst:262
msgid "Export"
msgstr "Exportar"

#: ../../user_manual/working_with_images.rst:265
msgid ""
"This is a bit of an odd one, but it opens a file, and forgets where you "
"saved it to, so that when pressing 'save' it asks you where to save it. This "
"is also called 'import' in other programs."
msgstr ""
"Esta é uma opção estranha, mas o que faz é abrir um ficheiro e esquece-se "
"onde o gravou, para que ao carregar em 'gravar' lhe pergunte onde o deseja "
"gravar. Isto também é chamado de 'importar' noutros programas."

#: ../../user_manual/working_with_images.rst:266
msgid "Open Existing Document As Untitled Document"
msgstr "Abrir um Documento Existe como Documento sem Nome"

#: ../../user_manual/working_with_images.rst:269
msgid "Create Copy from Current Image"
msgstr "Criar uma Cópia da Imagem Actual"

#: ../../user_manual/working_with_images.rst:269
msgid ""
"Makes a new copy of the current image. Similar to Open Existing Document As "
"Untitled Document, but then with already opened files."
msgstr ""
"Cria uma nova cópia da imagem actual. É semelhante à opção Abrir um "
"Documento Existente como um Documento sem Nome, mas para ficheiros já "
"abertos."

#: ../../user_manual/working_with_images.rst:272
msgid "Save Incremental Version"
msgstr "Gravar uma Versão Incremental"

#: ../../user_manual/working_with_images.rst:272
msgid ""
"Saves the current image as 'filename'\\_XXX.kra and switches the current "
"document to it."
msgstr ""
"Grava a imagem actual como 'ficheiro'\\_XXX.kra e mudar o documento actual "
"para ele."

#: ../../user_manual/working_with_images.rst:275
msgid ""
"Copies and renames the last saved version of your file to a back-up file and "
"saves your document under the original name."
msgstr ""
"Copia e muda o nome da última versão gravada do seu ficheiro numa cópia de "
"segurança e grava o seu documento com o nome original."

#: ../../user_manual/working_with_images.rst:276
msgid "Save Incremental Backup"
msgstr "Gravar uma Cópia de Segurança Incremental"

#: ../../user_manual/working_with_images.rst:279
msgid ""
"Since Krita's file format is compressed data file, in case of a corrupt or "
"broken file you can open it with archive managers and extract the contents "
"of the layers. This will help you to recover as much as possible data from "
"the file. On Windows, you will need to rename it to filename.zip to open it."
msgstr ""
"Dado que o formato do ficheiro do Krita é um ficheiro de dados comprimido, "
"no caso de um ficheiro danificado ou corrompido, podê-lo-á abrir com "
"gestores de pacotes e extrair o conteúdo das camadas. Isto ajudá-lo-á a "
"recuperar o máximo possível de dados do ficheiro. No Windows, terá de mudar "
"o nome dele para ficheiro.zip, para que o consiga abrir."
