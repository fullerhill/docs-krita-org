msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-08-02 03:06+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___tools___line.pot\n"

#: ../../<generated>:1
msgid "Preview"
msgstr "Preview"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/line_tool.svg\n"
"   :alt: toolline"

#: ../../<rst_epilog>:24
msgid ""
".. image:: images/icons/line_tool.svg\n"
"   :alt: toolline"
msgstr ""
".. image:: images/icons/line_tool.svg\n"
"   :alt: toolline"

#: ../../reference_manual/tools/line.rst:1
msgid "Krita's line tool reference."
msgstr ""

#: ../../reference_manual/tools/line.rst:11
msgid "Tools"
msgstr ""

#: ../../reference_manual/tools/line.rst:11
msgid "Line"
msgstr ""

#: ../../reference_manual/tools/line.rst:11
msgid "Straight Line"
msgstr ""

#: ../../reference_manual/tools/line.rst:16
msgid "Straight Line Tool"
msgstr "直线工具"

#: ../../reference_manual/tools/line.rst:18
msgid "|toolline|"
msgstr ""

#: ../../reference_manual/tools/line.rst:21
msgid ""
"This tool is used to draw lines. Click the |mouseleft| to indicate the first "
"endpoint, keep the button pressed, drag to the second endpoint and release "
"the button."
msgstr ""

#: ../../reference_manual/tools/line.rst:24
msgid "Hotkeys and Sticky Keys"
msgstr "快捷键和粘滞键"

#: ../../reference_manual/tools/line.rst:26
msgid ""
"To activate the Line tool from freehand brush mode, use the :kbd:`V` key. "
"Use other keys afterwards to constraint the line."
msgstr ""

#: ../../reference_manual/tools/line.rst:28
msgid ""
"Use the :kbd:`Shift` key while holding the mouse button to constrain the "
"angle to multiples of 15º. You can press the :kbd:`Alt` key while still "
"keeping the |mouseleft| down to move the line to a different location."
msgstr ""

#: ../../reference_manual/tools/line.rst:32
msgid ""
"Using the :kbd:`Shift` keys BEFORE pushing the holding the left mouse button/"
"stylus down will, in default Krita, activate the quick brush-resize. Be sure "
"to use the :kbd:`Shift` key after."
msgstr ""

#: ../../reference_manual/tools/line.rst:35
msgid "Tool Options"
msgstr "工具选项"

#: ../../reference_manual/tools/line.rst:37
msgid ""
"The following options allow you to modify the end-look of the straight-line "
"stroke with tablet-values. Of course, this only work for tablets, and "
"currently only on Paint Layers."
msgstr ""

#: ../../reference_manual/tools/line.rst:40
msgid "Use sensors"
msgstr "使用传感器"

#: ../../reference_manual/tools/line.rst:41
msgid ""
"This will draw the line while taking sensors into account. To use this "
"effectively, start the line and trace the path like you would when drawing a "
"straight line before releasing. If you make a mistake, make the line shorter "
"and start over."
msgstr ""

#: ../../reference_manual/tools/line.rst:43
msgid ""
"This will show the old-fashioned preview line so you know where your line "
"will end up."
msgstr ""
