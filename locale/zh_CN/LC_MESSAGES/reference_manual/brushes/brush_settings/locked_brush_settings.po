msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-08-16 17:04\n"
"Last-Translator: Guo Yunhe (guoyunhe)\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_reference_manual___brushes___brush_settings___locked_brush_settings."
"pot\n"

#: ../../<generated>:1
msgid "Unlock (Keep Locked)"
msgstr "解除锁定 (保留当前设置)"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:1
msgid "How to keep brush settings locked in Krita."
msgstr "如何在 Krita 里面锁定笔刷选项。"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:11
#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:16
msgid "Locked Brush Settings"
msgstr "锁定笔刷选项"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:19
msgid ""
"Normally, a changing to a different brush preset will change all brush "
"settings. Locked presets are a way for you to prevent Krita from changing "
"all settings. So, if you want to have the texture be that same over all "
"brushes, you lock the texture parameter. That way, all brush-preset you "
"select will now share the same texture!"
msgstr ""
"一般来说，当你切换到另一个笔刷预设时，整个笔刷选项编辑器里面的选项都会跟着发"
"生变化。如果你希望某几个选项在切换笔刷预设时不会发生变化，你可以锁定它们。例"
"如，你可以锁定纹理参数，这样不同的笔刷都会画出相同的纹理。"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:22
msgid "Locking a brush parameter"
msgstr "锁定笔刷选项"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:25
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_01.png"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:26
msgid ""
"To lock an option, |mouseright| the little lock icon next to the parameter "
"name, and set it to :guilabel:`Lock`. It will now be highlighted to show "
"it's locked:"
msgstr ""
"要锁定一个选项，可右键单击 |mouseright| 参数右边的“挂锁”图标，把它设为 :"
"guilabel:`锁定` 。该图标在锁定状态下会高亮显示："

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:29
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_02.png"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:30
msgid "And on the canvas, it will show that the texture-option is locked."
msgstr ""
"现在在画布上用不同笔刷作画，你会发现它们的纹理都被统一了。下图的左边为纹理选"
"项锁定前，右边为纹理选项锁定后。"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:33
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_04.png"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:35
msgid "Unlocking a brush parameter"
msgstr "解除锁定笔刷选项"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:37
msgid "To *unlock*, |mouseright| the icon again."
msgstr "要解除锁定一个选项，可再次右键单击 |mouseright| 挂锁图标。"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:40
msgid ".. image:: images/brushes/Krita_2_9_brushengine_locking_03.png"
msgstr ""

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:41
msgid "There will be two options:"
msgstr "这将弹出两个选项："

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:43
msgid "Unlock (Drop Locked)"
msgstr "解除锁定 (恢复预设设置)"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:44
msgid ""
"This will get rid of the settings of the locked parameter and take that of "
"the active brush preset. So if your brush had no texture on, using this "
"option will revert it to having no texture."
msgstr ""
"选中此项将放弃锁定的选项设置，恢复成当前活动笔刷预设的选项。例如，如果你之前"
"锁定了纹理，而当前笔刷预设本身是不带纹理的，选中此项将恢复该选项为不带纹理。"

#: ../../reference_manual/brushes/brush_settings/locked_brush_settings.rst:46
msgid "This will keep the settings of the parameter even though it's unlocked."
msgstr "选中此项则会在解锁后依然保持该选项的设置 (直到下次切换笔刷)。"
