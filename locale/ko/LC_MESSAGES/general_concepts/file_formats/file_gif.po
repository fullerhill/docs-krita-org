# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# Shinjo Park <kde@peremen.name>, 2019.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.2\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-07-19 03:24+0200\n"
"PO-Revision-Date: 2019-05-30 00:40+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../general_concepts/file_formats/file_gif.rst:1
#, fuzzy
#| msgid "The Gif file format in Krita."
msgid "The GIF file format in Krita."
msgstr "Krita에서 다루는 GIF 파일 형식입니다."

#: ../../general_concepts/file_formats/file_gif.rst:10
msgid "GIF"
msgstr "GIF"

#: ../../general_concepts/file_formats/file_gif.rst:10
msgid "*.gif"
msgstr "*.gif"

#: ../../general_concepts/file_formats/file_gif.rst:15
msgid "\\*.gif"
msgstr "\\*.gif"

#: ../../general_concepts/file_formats/file_gif.rst:17
#, fuzzy
#| msgid ""
#| ".gif is a file format mostly known for the fact that it can save "
#| "animations. It's a fairly old format, and it does its compression by :ref:"
#| "`indexing <bit_depth>` the colors to a maximum of 256 colors per frame. "
#| "Because we can technically design an image for 256 colors and are always "
#| "able save over an edited gif without any kind of extra degradation, this "
#| "is a :ref:`lossless <lossless_compression>` compression technique."
msgid ""
"``.gif`` is a file format mostly known for the fact that it can save "
"animations. It's a fairly old format, and it does its compression by :ref:"
"`indexing <bit_depth>` the colors to a maximum of 256 colors per frame. "
"Because we can technically design an image for 256 colors and are always "
"able save over an edited GIF without any kind of extra degradation, this is "
"a :ref:`lossless <lossless_compression>` compression technique."
msgstr ""
".gif 파일 형식은 애니메이션을 저장하는 형식으로 잘 알려져 있습니다. 이 형식"
"은 개발된지 오래 되었으며, 프레임당 최대 256색으로 색상을 :ref:`indexing "
"<bit_depth>`하여 압축합니다. 이론적으로 256색으로 이미지를 생성하고 GIF 파일"
"로 저장하면 손실이 발생하지 않기 때문에 :ref:`loseless "
"<lossless_compression>` 압축으로 취급됩니다."

#: ../../general_concepts/file_formats/file_gif.rst:19
msgid ""
"This means that it can handle most grayscale images just fine and without "
"losing any visible quality. But for color images that don't animate it might "
"be better to use :ref:`file_jpg` or :ref:`file_png`."
msgstr ""
"대부분의 회색조 이미지를 화질 손실 없이 처리할 수 있음을 의미합니다. 애니메이"
"션을 사용하지 않는 컬러 이미지에는 :ref:`file_jpg` 또는 :ref:`file_png` 형식"
"을 추천합니다."
