# Shinjo Park <kde@peremen.name>, 2019.
# JungHee Lee <daemul72@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-05-04 03:33+0200\n"
"PO-Revision-Date: 2019-05-21 01:18+0200\n"
"Last-Translator: Shinjo Park <kde@peremen.name>\n"
"Language-Team: Korean <kde-kr@kde.org>\n"
"Language: ko\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: Lokalize 18.12.3\n"

#: ../../reference_manual/preferences/color_management_settings.rst:1
msgid "The color management settings in Krita."
msgstr "Krita의 색상 관리 설정입니다."

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Preferences"
msgstr "환경 설정"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Settings"
msgstr "설정"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Color Management"
msgstr "색상 관리"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Color"
msgstr "색상"

#: ../../reference_manual/preferences/color_management_settings.rst:12
msgid "Softproofing"
msgstr "소프트프루핑"

#: ../../reference_manual/preferences/color_management_settings.rst:17
msgid "Color Management Settings"
msgstr "색상 관리 설정"

#: ../../reference_manual/preferences/color_management_settings.rst:20
msgid ".. image:: images/preferences/Krita_Preferences_Color_Management.png"
msgstr ".. image:: images/preferences/Krita_Preferences_Color_Management.png"

#: ../../reference_manual/preferences/color_management_settings.rst:21
msgid ""
"Krita offers extensive functionality for color management, utilising `Little "
"CMS <http://www.littlecms.com/>`_ We describe Color Management in a more "
"overall level here: :ref:`color_managed_workflow`."
msgstr ""
"Krita는`Little CMS <http://www.littlecms.com/>`를 사용하여 색상 관리를 위한 "
"광범위한 기능을 제공합니다. 여기에서 좀 더 전반적인 레벨의 색상 관리를 설명합"
"니다: :ref:`color_managed_workflow`."

#: ../../reference_manual/preferences/color_management_settings.rst:25
msgid "General"
msgstr "일반"

#: ../../reference_manual/preferences/color_management_settings.rst:28
msgid "Default Color Model For New Images"
msgstr "새 이미지의 기본 색상 모델"

#: ../../reference_manual/preferences/color_management_settings.rst:30
msgid "Choose the default model you prefer for all your images."
msgstr "모든 이미지에 대해 선호하는 기본 모델을 선택하십시오."

#: ../../reference_manual/preferences/color_management_settings.rst:33
msgid "When Pasting Into Krita From Other Applications"
msgstr "다른 프로그램에서 Krita로 붙여 넣을 때"

#: ../../reference_manual/preferences/color_management_settings.rst:35
msgid ""
"The user can define what kind of conversion, if any, Krita will do to an "
"image that is copied from other applications i.e. Browser, GIMP, etc."
msgstr ""
"다른 애플리케이션(예: 브라우저, 김프 등)에서 복사된 이미지에 대해 Krita가 수"
"행할 변환 유형을 정의할 수 있습니다."

#: ../../reference_manual/preferences/color_management_settings.rst:37
msgid "Assume sRGB"
msgstr "sRGB로 간주"

#: ../../reference_manual/preferences/color_management_settings.rst:38
msgid ""
"This option will show the pasted image in the default Krita ICC profile of "
"sRGB."
msgstr "이 옵션은 붙여 넣은 이미지를 기본 Krita sRGB ICC 프로필로 표시합니다."

#: ../../reference_manual/preferences/color_management_settings.rst:39
msgid "Assume monitor profile"
msgstr "모니터 프로필로 간주"

#: ../../reference_manual/preferences/color_management_settings.rst:40
msgid ""
"This option will show the pasted image in the monitor profile selected in "
"system preferences."
msgstr ""
"이 옵션은 붙여 넣은 이미지를 시스템 설정에서 선택한 모니터 프로필로 표시합니"
"다."

#: ../../reference_manual/preferences/color_management_settings.rst:42
msgid "Ask each time"
msgstr "매번 확인"

#: ../../reference_manual/preferences/color_management_settings.rst:42
msgid ""
"Krita will ask the user each time an image is pasted, what to do with it. "
"This is the default."
msgstr "Krita에 이미지를 붙여 넣을 때마다 사용자에게 묻습니다(기본값)."

#: ../../reference_manual/preferences/color_management_settings.rst:46
msgid ""
"When copying and pasting in Krita color information is always preserved."
msgstr "Krita에서 복사 및 붙여넣기 시 색상 정보는 항상 보존됩니다."

#: ../../reference_manual/preferences/color_management_settings.rst:49
msgid "Use Blackpoint Compensation"
msgstr "Blackpoint 보정 사용"

#: ../../reference_manual/preferences/color_management_settings.rst:51
msgid ""
"This option will turn on Blackpoint Compensation for the conversion. BPC is "
"explained by the maintainer of LCMS as following:"
msgstr ""
"이 옵션은 변환 시에 Blackpoint 보정을 켭니다. LCMS 관리자는 BPC를 다음과 같"
"이 설명합니다:"

#: ../../reference_manual/preferences/color_management_settings.rst:53
msgid ""
"BPC is a sort of \"poor man's\" gamut mapping. It basically adjust contrast "
"of images in a way that darkest tone of source device gets mapped to darkest "
"tone of destination device. If you have an image that is adjusted to be "
"displayed on a monitor, and want to print it on a large format printer, you "
"should realize printer can render black significantly darker that the "
"screen. So BPC can do the adjustment for you. It only makes sense on "
"Relative colorimetric intent. Perceptual and Saturation does have an "
"implicit BPC."
msgstr ""
"BPC는 일종의 \"불쌍한 사람의\" 영역 맵핑입니다. 기본적으로 소스 장치의 가장 "
"어두운 톤이 대상 장치의 가장 어두운 톤에 매핑되는 방식으로 이미지의 대비를 조"
"정합니다. 모니터에 표시되도록 조정된 이미지를 대형 프린터에서 인쇄하려는 경"
"우, 프린터가 화면보다 검은색을 상당히 어둡게 렌더링한다는 것을 알아야 합니"
"다. 그래서 BPC가 조정을 해줄 수 있습니다. 단지 상대적인 색도계의 의도에만 의"
"미가 있습니다. 통찰력과 포화도는 내포된 BPC를 가지고 있습니다."

#: ../../reference_manual/preferences/color_management_settings.rst:56
msgid "Allow LittleCMS optimizations"
msgstr "LittleCMS 최적화 허용"

#: ../../reference_manual/preferences/color_management_settings.rst:58
msgid "Uncheck this option when using Linear Light RGB or XYZ."
msgstr "Linear Light RGB 또는 XYZ를 사용할 경우 이 옵션을 해제하십시오."

#: ../../reference_manual/preferences/color_management_settings.rst:61
msgid "Display"
msgstr "디스플레이"

#: ../../reference_manual/preferences/color_management_settings.rst:63
msgid "Use System Monitor Profile"
msgstr "시스템 모니터 프로필 사용"

#: ../../reference_manual/preferences/color_management_settings.rst:64
msgid ""
"This option when selected will tell Krita to use the ICC profile selected in "
"your system preferences."
msgstr ""
"이 옵션을 선택하면 시스템 환경 설정에서 선택한 ICC 프로파일을 Krita에 사용하"
"게 됩니다."

#: ../../reference_manual/preferences/color_management_settings.rst:65
msgid "Screen Profiles"
msgstr "화면 프로필"

#: ../../reference_manual/preferences/color_management_settings.rst:66
msgid ""
"There are as many of these as you have screens connected. The user can "
"select an ICC profile which Krita will use independent of the monitor "
"profile set in system preferences. The default is sRGB built-in. On Unix "
"systems, profile stored in $/usr/share/color/icc (system location) or $~/."
"local/share/color/icc (local location) will be proposed. Profile stored in "
"Krita preference folder, $~/.local/share/krita/profiles will be visible only "
"in Krita."
msgstr ""
"연결된 화면 개수만큼 표시됩니다. 시스템 설정에서 선택한 모니터 프로필과 관계 "
"없이 Krita에서 사용할 ICC 프로필을 선택할 수 있습니다. 기본값은 내장 sRGB입니"
"다. Unix 시스템에서는 $/usr/care/color/icc(시스템 위치) 또는 $~/.local/share/"
"color/icc(로컬 위치)에 저장된 프로파일을 제안합니다. Krita 환경 설정 폴더"
"($~/.local/share/krita/profiles)에 저장된 프로필은 Krita에만 표시됩니다."

#: ../../reference_manual/preferences/color_management_settings.rst:68
msgid "Rendering Intent"
msgstr "렌더링 의도"

#: ../../reference_manual/preferences/color_management_settings.rst:68
msgid ""
"Your choice of rendering intents is a way of telling Littlecms how you want "
"colors mapped from one color space to another. There are four options "
"available, all are explained on the :ref:`icc_profiles` manual page."
msgstr ""
"렌더링 의도의 선택은 Littlecms에게 한 색상 공간에서 다른 색상 공간으로 매핑"
"된 색상을 원하는 방식으로 알려주는 방법입니다. 네 가지 옵션이 있으며, 모두 :"
"ref:`icc_profiles` 매뉴얼 페이지에 설명되어 있습니다."

#: ../../reference_manual/preferences/color_management_settings.rst:71
msgid "Softproofing options"
msgstr "소프트 프루핑 옵션"

#: ../../reference_manual/preferences/color_management_settings.rst:73
msgid ""
"These allow you to configure the *default* softproofing options. To "
"configure the actual softproofing for the current image, go to :"
"menuselection:`Image --> Image Properties --> Softproofing` ."
msgstr ""
"이 옵션을 사용하면 *기본값* 소프트 프루핑 옵션을 구성할 수 있습니다. 현재 이"
"미지에 대해 실제 소프트 프루핑을 구성하려면 메뉴 선택: 이미지 --> 이미지 속"
"성 --> 소프트 프루핑으로 이동하십시오."

#: ../../reference_manual/preferences/color_management_settings.rst:75
msgid ""
"For indepth details about how to use softproofing, check out :ref:`the page "
"on softproofing <soft_proofing>`."
msgstr ""
"소프트프루핑 사용 방법에 대한 자세한 내용은 다음을 참조하십시오. :ref:`소프트"
"프루핑에 관한 페이지 <soft_proofing>`."
